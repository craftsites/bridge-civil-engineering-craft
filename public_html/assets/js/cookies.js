function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}



function show_cookie_policy()
{


    $('body').addClass('extra_footer_padding');

    $('body')
        .append(


            "<div id='cookie-info'>"
            + "<div class='text_cookie_footer'>"
            + "<div class='cookie_1'>"
            + "<p>By continuing to browse or by clicking “Accept All Cookies,” you agree to the storing of first- and third-party cookies on your device.</p>"
            + "</div>"
            + "<div class='cookie_buttons'>"
            + "<div class='cookie_2'><button  onclick='show_policy_window();' class='btn button-cookie-footer'>READ OUR POLICY</button></div>"
            + "<div class='cookie_3'><button id='accept' class='btn button-cookie-footer'  onclick='hide_cookie_window();'>ACCEPT ALL</button></div>"
            + "</div>"
            + "</div>"
            + "</div>"
        );
    $('#cookie-info').fadeIn(500);
    $('#cookie-info').slideDown(100);


}

function hide_cookie_window()
{
    $('body').removeClass('extra_footer_padding');
    var today = new Date();
    var expires = 60 * 1000 * 60 * 60 * 24;
    var expires_date = new Date( today.getTime() + (expires) );
    document.cookie = "Pop_up=displayed; expires=" + expires_date;
    $('#cookie-info').slideUp(100);

}

function show_policy_window()
{
    var url ="/privacy-policy";
    window.open(url, "_blank");

}

function close_window()
{
    $('#policy-window').slideUp(500);
    $('#policy-window').fadeOut(500);
    $('#page-mask').fadeOut(1000);

}

